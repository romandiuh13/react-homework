import React from 'react';
import './App.css';
import Logo from "./client/logo/component/logo";
import BookApp from "./client/bookApp";


function App() {
  return (
    <div className="container mt-4">
      <Logo />
      <BookApp />
    </div>
  );
}

export default App;
