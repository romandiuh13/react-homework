import React, {Component} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faBackspace} from "@fortawesome/free-solid-svg-icons";

class Table extends Component {

    render(){
        const {list, onDeleteBook, onEditBook} = this.props;
        const bookTable = list.map(({title, author, isbn}, index) => (
            <tr>
                <td>{title}</td>
                <td>{author}</td>
                <td>{isbn}</td>
                <td>
                    <a><FontAwesomeIcon icon={faEdit} onClick={()=>onEditBook(index)}/></a>
                    <a><FontAwesomeIcon icon={faBackspace}  onClick={()=>onDeleteBook(index)}/></a>
                </td>
            </tr>
        ))
     return (
         <table className='table table-striped mt-2'>
             <thead>
                 <tr>
                     <th>Title</th>
                     <th>Author</th>
                     <th>ISBN#</th>
                     <th>Action</th>
                 </tr>
             </thead>
             <tbody id='book-list'>
             {bookTable}
             </tbody>
         </table>
     )

    }
}
export default Table