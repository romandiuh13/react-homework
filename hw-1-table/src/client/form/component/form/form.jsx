import React, {Component} from "react";
import Input from "../../../../shared/component/input";
import {fields} from "../../../../shared/component/input/fields";


class Form extends Component {
    state = {
        title: '',
        author: '',
        isbn: '',
    }

    handleChange = ({target}) => {
        const {name} = target;
        this.setState({[name]: target.value});
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const {...newBook} = this.state;
        const {onSubmit} = this.props;
        onSubmit(newBook);
    }

    render() {
        const {title, author, isbn, submit} = fields;
        const {handleSubmit, handleChange} = this;
        return (
            <form id='add-book-form' onSubmit={handleSubmit}>

                <div className='form-group'>
                    <label htmlFor='title'>Title</label>
                    <Input {...title} value={this.state.title} handleChange={handleChange}/>
                </div>

                <div className='form-group'>
                    <label htmlFor='author'>Author</label>
                    <Input {...author} value={this.state.author} handleChange={handleChange}/>
                </div>

                <div className='form-group'>
                    <label htmlFor='isbn'>#ISBN</label>
                    <Input {...isbn} value={this.state.isbn} handleChange={handleChange}/>
                </div>

                <Input {...submit} />
            </form>

        )
    }
}

export default Form