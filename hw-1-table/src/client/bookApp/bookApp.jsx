import Form from "../form/component/form";
import BookCount from "../book-count/component";
import Table from "../table/component";
import React, {Component} from "react";

class BookApp extends Component {

    state = {
        bookList: [{
            title: 'Garry Potter',
            author: 'j.Rouling',
            isbn: '#1234312'
        },
            {
                title: 'Garry Potter and Prison Azkaban',
                author: 'j.Rouling',
                isbn: '#1234312'
            }
        ]
    }

    onAddBook = (newBook) => {
        this.setState(({bookList}) => {
            const newBookList = [...bookList];
            newBookList.push(newBook);

            return {
                bookList: newBookList
            }
        })
    }

    onEditBook = (index)=>{
        console.log('working')
    }

    onDeleteBook = (index)=>{
        this.setState(({bookList})=>{
            const newBookList = [...bookList]
            newBookList.splice(index, 1);
            return{
                bookList: newBookList
            }
        })
    }

    render() {

        return (
            <>
                <Form onSubmit={this.onAddBook}/>
                <BookCount count={this.state.bookList.length}/>
                <Table list={this.state.bookList} onDeleteBook={this.onDeleteBook} onEditBook={this.onEditBook}/>
            </>
        )
    }

}

export default BookApp