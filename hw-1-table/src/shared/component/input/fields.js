export const fields = {
    title: {
        type: "text",
        placeholder: 'Введите название книги',
        name: "title",
        className: 'form-control'
    },
    author: {
        type: "text",
        placeholder: "Введите автора книги",
        name: "author",
        className: 'form-control'
    },
    isbn: {
        type: "text",
        placeholder: "Введите #ISBN книги",
        name: "isbn",
        className: 'form-control'
    },
    submit: {
        type: "submit",
        value: 'Add book',
        className:'btn btn-primary'

    }
};